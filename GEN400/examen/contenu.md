---
fontsize: 12pt
lang: fr
lieu: |
      | UNIVERSITÉ DE SHERBROOKE
      | Faculté de génie
      | Département de génie électrique et génie informatique
title: Examen -- Les déchets électroniques
course-title: |
              | Ingénieur et société
              | GEN400
presented-to: Équipe de formateurs de la session S3
author: Matthieu Daoust, daom2504
date: Sherbrooke, 17 août 2021
toc: true
geometry: margin=1.5in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{2}
    - \usepackage[all]{nowidow}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \cfoot{\thepage}
---

# Introduction

## Sujet amené

Depuis la commercialisation des produits électroniques, la population en consomme. Lorsque la fin du cycle de vie de l'appareil approche et que le consommateur ne trouve pas de solution de récupération, ces objets électroniques produisent des déchets. Ces déchets finissent parfois dans des dépotoirs à ciel ouvert et peuvent générer des problèmes dans des communautés à travers le monde. Il serait intéressant de trouver une solution pour l'élimination responsable des déchets.

## Sujet posé

Afin d'éliminer les déchets de façon responsable, nous allons explorer la possibilité d'ajouter une taxe à l'achat de produits électroniques difficilement recyclable pour financer des centres de recyclage.

## Sujet divisé

Dans ce texte, il sera question de la présentation synthétisée et vulgarisée de l'ajout d'une taxe à l'achat de produits électroniques difficilement recyclable pour financer des centres de recyclage et de la justification du projet selon des valeurs fondamentales du développement durable.

\newpage

# Développement

## Présentation synthétisée et vulgarisée de la solution

La création d'une taxe à l'achat de produits électroniques difficilement recyclable dans le but de financer des centres de recyclages permet d'améliorer la situation à plusieurs niveaux de la gestion des déchets d'équipement électrique et électronique (DEEE).

En effet, appliquer une taxe seulement sur les articles les plus polluants aura pour effet de sensibiliser les consommateurs qu'il existe des alternatives qui se recyclent mieux.

Aussi, l'augmentation du coût après taxes des articles qui génèrent plus de déchets pourrait avoir comme impact de diminuer la quantité d'articles achetée, ce qui ferait diminuer la quantité d'articles produite, ce qui ferait diminuer à la source la production des DEEE.

Enfin, ces nouvelles lois et règlements pourraient encourager les producteurs à faciliter le recyclage de leurs appareils, entre autres en finançant des centres de recyclages et en diminuant les taxes sur les produits facilement recyclables.

## Justification du projet pilote

Selon les quatre valeurs fondamentales du développement durable (Villeneuve 1998), le projet pilote répondrait parfaitement ou en partie à ces quatre valeurs.

### Forme de développement écologiquement viable

Ce projet serait écologiquement viable, car moins de ressources seraient utilisées. En effet, la taxe sur les produits qui se recyclent mal pourrait encourager les consommateurs à moins consommer de ces biens, ce qui provoquerait une baisse de la production de ces biens. Une baisse de la production serait bénéfique pour l'écologie. Ce projet répond donc parfaitement à une forme de développement écologiquement viable.

### Forme de développement capable de réduire les disparités riches-pauvres

Ce projet ne permettrait pas totalement de réduire les disparités entre les riches et les pauvres. En effet, malgré le fait que la taxe serait la même pour tout le monde, cette taxe pourrait avoir un impact moindre pour les personnes à revenu élevé qui vont quand même pouvoir se permettre de s'acheter des articles électroniques malgré la taxe. Ce projet répond donc en partie à une forme de développement capable de réduire les disparités riches-pauvres.

### Forme de développement économiquement efficace

Ce projet serait économiquement efficace. En effet, la taxe qui permet de financer le recyclage des appareils électriques serait proportionnel à la quantité de matériel à recycler. Aussi, la taxe serait un incitatif économique pour les entreprises pour créer des produits facilement recyclables. Ce projet répond donc parfaitement à une forme de développement économiquement efficace.

### Forme de développement socialement équitable

Ce projet serait socialement équitable. En effet, le financement des centres de récupérations locaux aurait pour effet de diminuer la quantité de DEEE exporté dans des pays qui opèrent des dépotoirs à ciel ouvert. Cela permettrait de diminuer les problèmes de santé des populations qui travaillent dans ces dépotoirs distants. De plus, cela permettrait de créer des emplois locaux de qualité. Ce projet répond donc parfaitement à une forme de développement socialement équitable.

\newpage

# Conclusion

## Rappel

En conclusion, il serait bénéfique, d'un point de vue du développement durable, de créer une taxe à l'achat de produits électroniques difficilement recyclable pour financer des centres de recyclage. En effet, il y aurait beaucoup d'impacts positifs écologique, égalitaire, économique et social et très peu d'impact négatif.

## Ouverture

Et vous, seriez-vous prêt à payer plus cher pour vos équipements électriques et électroniques?

\newpage

# Bibliographie

[1] BENSEBAA, Faouzi et Fabienne BOUDIER, _Gestion des déchets dangereux et responsabilité sociale
des firmes : le commerce illégal de déchets électriques et électroniques_, Développement durable et
territoires [En ligne] http://developpementdurable.revues.org/4823

[2] GRAVEL, P., _Les déchets électroniques s’amoncellent à la vitesse grand V_, Le Devoir [En ligne] https://www.ledevoir.com/societe/science/515339/rebus-electroniques

[3] Villeneuve, Claude. 1998. _Qui a peur de l’an 2000?  Guide d’éducation relative à l’environnementpour le développement durable._ Sainte-Foy: Éditions Multimondes et UNESCO. 303 p.
