---
fontsize: 12pt
lang: fr
lieu: |
      | UNIVERSITÉ DE SHERBROOKE
      | Faculté de génie
      | Département de génie électrique et génie informatique
title: Travail individuel sur l'implantation de caméras de surveillances sur le campus de l'Université de Sherbrooke
course-title: |
              | Ingénieur et société
              | GEN400
presented-to: Équipe de formateurs de la session S3
author: Matthieu Daoust, daom2504
date: Sherbrooke, 18 juillet 2021
toc: true
geometry: margin=1.5in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{2}
    - \usepackage[all]{nowidow}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \cfoot{\thepage}
---

# Introduction

## Sujet amené

Depuis que les êtres humains commettent des méfaits, les propriétaires souhaitent connaître l'identité des malfaiteurs. Les universités ont eu longtemps recours à des agents de sécurité sur leurs campus. Depuis l'invention des caméras de surveillances, il est plus facile pour ceux qui les utilisent de retracer les malfaiteurs, mais leur utilisation peux soulever des enjeux éthiques.

## Sujet posé

On se demande si l'Université de Sherbrooke devrait installer des caméras de surveillances sur leur campus.

## Sujet divisé

Dans ce texte, il sera question de la description du cas choisi, de la description de la contreverse, des thématiques impliquées et du diagnostic en fonction de l'analyse de la controverse.

\newpage

# Développement

## Description du cas choisi

### Historique

Depuis qu'il est possible d'utiliser des caméras de surveillances, il est possible de retracer les malfaiteurs plus facilement avec moins de ressources humaines, mais cela implique que tout les humains qui passent devant les caméras sont maintenant considéré comme suspect potentiels.

### Groupes et acteurs impliqués

Plusieurs acteurs sont impliqués dans le projet d'implantation de caméras de surveillances sur le campus de l'Université de Sherbrooke.

Il y a, entre autre, les techniciens qui font l'installation, les agents qui avaient comme tâches d'identifier les méfaits en direct, les agents qui vont devoir identifier les suspects après que les méfaits ait été commis, et les humains qui vont passer devant les caméras.

\newpage

### Technologies utilisées

Habituellement, pour effectuer de la surveillance au moyen de caméras de surveillance, des caméras de surveillances sont installés à plusieurs endroit stratégiques sur le campus.

Ces caméras sont souvent reliés à un enregistreur central, qui permet de conserver les images pendant un certain temps.

Pour visionner les enregistrements, du matériel de visionnement est aussi utilisé.

### Définition des concepts techniques

Un enregistrement permet de conserver une preuve visuelle sur un plus grand territoire, de façon plus fiable, avec moins de ressources humaines et sur une plus grande période temporelle.

### Lieu d'implantation ou de transfert

Les caméras de surveillances devrais être installé aux endroits qui permettraient plus facilement d'identifier les malfaiteurs.

## Description de la controverse

### Historique

La controverse découle du fait que les données qui permettent de retracer les malfaiteurs peuvent être conservé beaucoup plus longtemps et pour un plus grand territoire qu'avec une surveillance «classique» avec des agents de surveillances.

### Positions adverses

Bien qu'une surveillance formelle et informelle permet souvent de renforcer le sentiment de sécurité, elle peux entraîner une atteinte aux droits et liberté individuelles.

### Groupes et acteurs impliqués dans la controverse

Les acteurs impliqués dans la controverse sont principalement les humains qui passent devant les caméras et qui ne font rien de mal.

### Valeurs en conflit

Parmi les valeurs en conflit dans cette controverse, il y a les libertés individuelles, et le besoin de connaître l'identité des malfaiteurs.

\newpage

## Thématiques impliquées

### Rapport au temps

On remarque clairement le rapport au temps «Chronos» dans les enregistrements. En effet, avec la surveillance électronique, on est en mesure de mesurer précisément le temps de chaque actions de chaques humains qui passent devant une caméra.

On peux aussi remarquer le rapport au temps «Kairos» dans la façon dont les agents de sécurité recueillent l'information des enregistrements. En effet, afin d'obtenir de l'information pertinente, ils doivent chercher au bon moment.

Chez les individus filmés, on remarque l'absence du rapport au temps «Scholé» lorsqu'ils sont devant une caméra. En effet, ils sont constamment surveillé, sans possibilité d'obtenir une pause.

### Organisation du travail

On peux clairement voir que l'utilisation de caméras de surveillances modifie le travail des agents de sécurité. En effet, avant l'arrivée des caméras des surveillances, ils devaient être très vigilants partout sur les campus et être à l'affût du moindre méfaits. Après l'arrivée des caméras de surveillances, ils peuvent se permettre de moins surveiller, puis qu'ils savent qu'ils vont pouvoir prendre le temps de regarder les enregistrements dans le futur si un méfait se produit.

### Transfert technologique

On peux remarquer un transfert technologique. En effet, les caméras vidéos ont été réinventé pour en faire un objet de surveillance.

## Diagnostic en fonction de l'analyse de la controverse

Ce projet n'est acceptable qu'à la condition que des modifications soient apportées. En effet, ils doivent s'assurer que le besoin de surveillance est plus utile que préjudiciable aux personnes dont les images sont captées.

\newpage

# Conclusion

## Résumé

En conclusion, les caméras de surveillances peuvent être utilisés, seulement s'il est démontré qu'elle règlent plus de problèmes qu'elles n'en causent.

## Ouverture

Seriez-vous prêt à circuler dans un zone protégé par des caméras de surveillances?

\newpage

# Bibliographie

## Sites internet

[1] https://www.usherbrooke.ca/immeubles/la-securite/

## Documents

[2] https://en.wikipedia.org/wiki/Mass_surveillance_in_the_United_States

[3] https://fr.wikipedia.org/wiki/Vid%C3%A9osurveillance

[4] https://fr.wikipedia.org/wiki/Surveillance

[5] «Mesures à mettre en place pour favoriser le sentiment de sécurité en contexte de pandémie de COVID-19 et de déconfinement partiel», _INSPQ_. [https://www.inspq.qc.ca/publications/3147-sentiment-securite-pandemie-et-deconfinement-covid19](https://www.inspq.qc.ca/publications/3147-sentiment-securite-pandemie-et-deconfinement-covid19)

[6] «La vidéosurveillance : Conseils pratiques à l'intention des organismes publics et des entreprises», _Commission d'accès à l'information du Québec_. [https://www.cai.gouv.qc.ca/la-videosurveillance/](https://www.cai.gouv.qc.ca/la-videosurveillance/)
